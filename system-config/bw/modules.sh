# module and build environment developed by Michael Howard
# https://groups.google.com/forum/#!msg/hoomd-users/dCps_YmghmE/i04cc8w5DgAJ

export GLOTZPORTS_SP=${GLOTZPORTS_ROOT}/lib/python3.4/site-packages
export GLOTZPORTS_HOST='bw'
export GLOTZPORTS_CMAKE_FLAGS='-DENABLE_CUDA=ON -DENABLE_MPI=ON -DCUDA_ARCH_LIST="35" -DCMAKE_C_FLAGS="-march=native -fPIC" -DCMAKE_CXX_FLAGS="-march=native -fPIC"'
export PYTHONPATH=${GLOTZPORTS_SP}

module swap PrgEnv-cray PrgEnv-gnu
module load cmake
module load bwpy/0.3.0
module load cudatoolkit/7.5.18-1.0502.10743.2.1

export CRAYPE_LINK_TYPE=dynamic
export CRAY_ADD_RPATH=yes

export CC=cc
export CXX=CC

export CPATH=${BWPY_DIR}/usr/include
export LIBRARY_PATH=${BWPY_DIR}/lib64:${BWPY_DIR}/usr/lib64
export LD_LIBRARY_PATH=${BWPY_DIR}/lib64:${BWPY_DIR}/usr/lib64:${LD_LIBRARY_PATH}

export GLOTZPORTS_PYTHON=`which python`
