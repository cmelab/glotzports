export GLOTZPORTS_SP=${GLOTZPORTS_ROOT}/lib64/python3.4/site-packages
export PYTHONPATH=${GLOTZPORTS_SP}
export GLOTZPORTS_HOST='vislab'
export GLOTZPORTS_GCC_FLAGS='-march=ivybridge -mmmx -msse -msse2 -msse3 -mssse3 -mcx16 -msahf -maes -mpclmul -mpopcnt -mavx -msse4.2 -msse4.1 -mrdrnd -mf16c -mfsgsbase -mfxsr -mxsave -mxsaveopt --param l1-cache-size=32 --param l1-cache-line-size=64 --param l2-cache-size=25600 -mtune=ivybridge -fstack-protector-strong'
export GLOTZPORTS_CMAKE_FLAGS='-DENABLE_CUDA=ON -DENABLE_MPI=ON -DCMAKE_CXX_FLAGS="$GLOTZPORTS_GCC_FLAGS" -DCMAKE_C_FLAGS="$GLOTZPORTS_GCC_FLAGS"'
export GLOTZPORTS_PYTHON=`which python3`
