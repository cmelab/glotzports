export GLOTZPORTS_SP=${GLOTZPORTS_ROOT}/lib/python2.7/site-packages
export PYTHONPATH=${GLOTZPORTS_SP}
export GLOTZPORTS_HOST='comet-gpu'
export GLOTZPORTS_CMAKE_FLAGS='-DENABLE_MPI=ON -DENABLE_CUDA=ON -DCMAKE_CXX_FLAGS=-march=native -DCMAKE_C_FLAGS=-march=native'

module unload intel
module load python
module load gnu
module load mvapich2_ib
module load gnutools
module load scipy
module load cmake
module load cuda/7.0
module load boost

export CC=`which gcc`
export CXX=`which g++`

export GLOTZPORTS_PYTHON=`which python`
export TBB_LINK=${GLOTZPORTS_ROOT}/lib
export TBB_INC=${GLOTZPORTS_ROOT}/include
