export GLOTZPORTS_SP=${GLOTZPORTS_ROOT}/lib/python3.6/site-packages
export PYTHONPATH=${GLOTZPORTS_SP}
export GLOTZPORTS_HOST='xstream'
export GLOTZPORTS_CMAKE_FLAGS='-DENABLE_MPI=ON -DENABLE_CUDA=ON -DCMAKE_CXX_FLAGS=-march=native -DCMAKE_C_FLAGS=-march=native -DSINGLE_PRECISION=ON'

module purge
module load foss/2015.05
module load Python/3.6.0
module load mvapich2_gnu/2.0.1_gnu48
module load CUDA/8.0.61
#Boost only built against python2.7
#Boost/1.61.0-Python-2.7.9

export CC=`which gcc`
export CXX=`which g++`

export GLOTZPORTS_PYTHON=`which python`
export TBB_LINK=${GLOTZPORTS_ROOT}/lib
export TBB_INC=${GLOTZPORTS_ROOT}/include
