export GLOTZPORTS_SP=${GLOTZPORTS_ROOT}/lib/python2.7/site-packages
export GLOTZPORTS_HOST='psg'
export GLOTZPORTS_CMAKE_FLAGS='-DENABLE_CUDA=ON -DENABLE_MPI=ON -DCMAKE_CXX_FLAGS=-march=native -DCMAKE_C_FLAGS=-march=native'
export PYTHONPATH=${GLOTZPORTS_SP}

module load PrgEnv/GCC+MPICH/2016-07-22
module load cmake/3.6.0
module load mpich/3.1.3
module load cuda/8.0.44

export GLOTZPORTS_PYTHON=`which python`
