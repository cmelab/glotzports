export GLOTZPORTS_SP=${GLOTZPORTS_ROOT}/lib/python2.7/site-packages
export GLOTZPORTS_HOST='eos'
export GLOTZPORTS_CMAKE_FLAGS='-DENABLE_CUDA=OFF -DENABLE_MPI=ON -DCMAKE_CXX_FLAGS=-march=native -DCMAKE_C_FLAGS=-march=native'
export PYTHONPATH=${GLOTZPORTS_SP}

module unload PrgEnv-intel
module load PrgEnv-gnu
module load cmake
module load git
module load python/2.7.9
module load python_numpy/1.9.2
module load python_setuptools/18.7
module load python_pip/7.1.2
module load boost/1.60.0
# need gcc first on the search path
module unload gcc
module load gcc/4.9.3

export GLOTZPORTS_PYTHON=`which python`
export TBB_LINK=${GLOTZPORTS_ROOT}/lib
export TBB_INC=${GLOTZPORTS_ROOT}/include
export LD_LIBRARY_PATH=${GLOTZPORTS_ROOT}/lib:${LD_LIBRARY_PATH}
