export GLOTZPORTS_SP=${GLOTZPORTS_ROOT}/lib/python2.7/site-packages
export PYTHONPATH=${GLOTZPORTS_SP}
export GLOTZPORTS_HOST='titan'
export GLOTZPORTS_CMAKE_FLAGS='-DENABLE_CUDA=ON -DENABLE_MPI=ON -DCUDA_ARCH_LIST="35" -DCMAKE_C_FLAGS="-march=native -fPIC" -DCMAKE_CXX_FLAGS="-march=native -fPIC"'

module unload PrgEnv-pgi
module load PrgEnv-gnu
module load cmake3/3.6.1
module load git/2.4.6
module load cudatoolkit/7.5.18-1.0502.10743.2.1
module load python/3.5.1
module load python_numpy/1.9.2
module load python_setuptools/23.0.0
module load tbb/43
module load boost/1.57.0
# need gcc first on the search path
module unload gcc/4.9.3
module load gcc/4.9.3

export TBB_LINK=/lustre/atlas/sw/tbb/43/sles11.3_gnu4.8.2/source/build/linux_intel64_gcc_cc4.8.2_libc2.11.3_kernel3.0.101_release/
export TBB_INC=/lustre/atlas/sw/tbb/43/sles11.3_gnu4.8.2/source/include

export GLOTZPORTS_PYTHON=`which python`
