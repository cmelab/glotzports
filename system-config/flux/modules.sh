export GLOTZPORTS_SP=${GLOTZPORTS_ROOT}/lib/python3.5/site-packages
export GLOTZPORTS_HOST='flux'
export GLOTZPORTS_CMAKE_FLAGS='-DENABLE_CUDA=ON -DENABLE_MPI=ON -DCMAKE_CXX_FLAGS=-march=native -DCMAKE_C_FLAGS=-march=native'
export PYTHONPATH=${GLOTZPORTS_SP}

module load cmake/3.5.2
module load cuda/7.5
module load gcc/4.8.5

module load python-dev/3.5.2
module load numpy-dev/1.11.1
module load scipy-dev/0.17.1
module load openmpi/1.10.2/gcc/4.8.5
module load boost-dev/1.61.0
module load mpi4py-dev/2.0.0
module load intel-tbb/4.4-u5

export GLOTZPORTS_PYTHON=`which python3`
export GLOTZPORTS_MAKE_JOBS=4
