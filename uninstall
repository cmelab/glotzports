#! /bin/bash

umask 022

# ensure that glotzports is active
if [ -z ${GLOTZPORTS_ROOT+x} ]; then
    echo "Error: No active glotzports environment"
    exit 1
fi

# exit on errors
set -u
set -e

# check that the user provided arguments
if [ $# -lt 1 ]
  then
    echo "Usage: ./uninstall package [package package ...]"
    echo "where package is a package name"
    exit 1
fi

package_list="$@"

# for each package
for package in ${package_list}
do
    # determine if the package is installed
    if grep -q "^${package}=" ${GLOTZPORTS_ROOT}/installed-packages; then

        # determine which version is installed
        install_line=`grep "^${package}=" ${GLOTZPORTS_ROOT}/installed-packages`

        if [ ! -f "packages/$package/uninstall.sh" ]
        then
            echo "Error: $package is not a glotzports package."
            exit 1
        fi

        # uninstall it and remove the line from the installed-packages file
        echo "Uninstalling $install_line..."
        bash packages/$package/uninstall.sh
        sed -i "/^${install_line}/d" ${GLOTZPORTS_ROOT}/installed-packages
        echo "Done uninstalling $install_line."
        echo ""

    else
        echo "Error: $package is not installed"
        exit 1
    fi
done
