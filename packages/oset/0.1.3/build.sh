#!/bin/bash
set -e
set -u

# version to build
ver="0.1.3"

# setup name variables
name="oset"
repo_name="${name}"
tag="v${ver}"

# install
pip install ${name}==${ver} --no-deps --target="${GLOTZPORTS_SP}"
