#!/bin/bash
set -e
set -u

# version to build
ver="145123f"

# setup name variables
name="peact"
repo_name="${name}"
tag="${ver}"
git_url="ssh://git@bitbucket.org/glotzer/${repo_name}.git"

# install
pip install git+${git_url}@${tag} --no-deps --target="${GLOTZPORTS_SP}"
