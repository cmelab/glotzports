#!/bin/bash
set -e
set -u

# Set the following environment vars before running build.sh
# GLOTZPORTS_SP - installation prefix python site packages dir
# GLOTZPORTS_ROOT - root installation prefix
# GLOTZPORTS_CMAKE_FLAGS - extra configure flags to pass to cmake when configuring hoomd

# versions to build
tag=v1.0.1

# setup name variables
name="gsd"
repo_name="${name}"
ver="${tag}"
git_url="https://bitbucket.org/glotzer/${repo_name}.git"
GLOTZPORTS_MAKE_JOBS="${GLOTZPORTS_MAKE_JOBS:=20}"

# build directory
TMPROOT=$(mktemp -d /tmp/${USER}.${name}-${ver}.XXXXXX)

echo "Building ${name}/${ver}"

# checkout code
git clone -n ${git_url} ${TMPROOT}/${name}
cd ${TMPROOT}/${name}
git checkout ${tag}

# install
python setup.py install --prefix=${GLOTZPORTS_ROOT}

# clean up tmp dirs
rm -rf ${TMPROOT}
