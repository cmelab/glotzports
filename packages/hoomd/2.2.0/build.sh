#!/bin/bash
set -e
set -u

# Set the following environment vars before running build.sh
# GLOTZPORTS_SP - installation prefix python site packages dir
# GLOTZPORTS_CMAKE_FLAGS - extra configure flags to pass to cmake when configuring hoomd

# versions to build
tag=v2.2.0

# setup name variables
name="hoomd"
repo_name="hoomd-blue"
ver="${tag}"
git_url="https://bitbucket.org/glotzer/${repo_name}.git"
GLOTZPORTS_MAKE_JOBS="${GLOTZPORTS_MAKE_JOBS:=20}"

# build directory
TMPROOT=$(mktemp -d /tmp/${USER}.${name}-${ver}.XXXXXX)

echo "Building ${name}/${ver}"

# checkout code
git clone -n ${git_url} ${TMPROOT}/${name}
cd ${TMPROOT}/${name}
git checkout ${tag}

# configure and install hoomd
mkdir build
cd build
eval cmake ../ -DCMAKE_INSTALL_PREFIX=${GLOTZPORTS_SP} -DBUILD_TESTING=off -DPYTHON_EXECUTABLE=${GLOTZPORTS_PYTHON} ${GLOTZPORTS_CMAKE_FLAGS}
make install -j${GLOTZPORTS_MAKE_JOBS}

# clean up tmp dirs
rm -rf ${TMPROOT}
