#!/bin/bash
set -e
set -u

# Set the following environment vars before running build.sh
# GLOTZPORTS_SP - installation prefix python site packages dir
# GLOTZPORTS_CMAKE_FLAGS - extra configure flags to pass to cmake when configuring hoomd

# versions to build
hoomd_tag=v2.0.3

# setup name variables
name="hoomd"
repo_name="hoomd-blue"
ver="${hoomd_tag}"
git_url="https://bitbucket.org/glotzer/${repo_name}.git"
GLOTZPORTS_MAKE_JOBS="${GLOTZPORTS_MAKE_JOBS:=20}"

# build directory
TMPROOT=$(mktemp -d /tmp/${USER}.${name}-${ver}.XXXXXX)

echo "Building ${name}/${ver}"

# checkout code
git clone -n ${git_url} ${TMPROOT}/hoomd
cd ${TMPROOT}/hoomd
git checkout ${hoomd_tag}

# configure and install hoomd
mkdir build
cd build
eval cmake ../ -DCMAKE_INSTALL_PREFIX=${GLOTZPORTS_SP} -DBUILD_TESTING=off ${GLOTZPORTS_CMAKE_FLAGS}
make install -j${GLOTZPORTS_MAKE_JOBS}

# clean up tmp dirs
rm -rf ${TMPROOT}
