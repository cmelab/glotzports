#!/bin/bash
set -e
set -u

# version to build
ver="0.6.3"

# setup name variables
name="nglview"
repo_name="${name}"
tag="v${ver}"

# install
pip install ${name}==${ver} --no-deps --target="${GLOTZPORTS_SP}"
