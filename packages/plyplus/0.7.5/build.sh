#!/bin/bash
set -e
set -u

# version to build
ver="0.7.5"

# setup name variables
name="PlyPlus"
repo_name="${name}"
tag="v${ver}"

# install
pip install ${name}==${ver} --no-deps --target="${GLOTZPORTS_SP}"
