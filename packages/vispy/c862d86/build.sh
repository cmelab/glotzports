#!/bin/bash
set -e
set -u

# version to build
ver="c862d86"

# setup name variables
name="vispy"
repo_name="${name}"
tag="${ver}"
git_url="https://github.com/vispy/${repo_name}.git"

# install
pip install git+${git_url}@${tag} --no-deps --target="${GLOTZPORTS_SP}"
