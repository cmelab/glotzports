#!/bin/bash
set -e
set -u

# uninstall hoomd
rm -rfv ${GLOTZPORTS_ROOT}/lib/vmd
rm -rfv ${GLOTZPORTS_ROOT}/bin/vmd
