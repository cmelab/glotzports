#!/bin/bash
set -e
set -u

# Set the following environment vars before running build.sh
# GLOTZPORTS_ROOT - installation prefix

# this script requires that vmd-1.9.3.bin.LINUXAMD64-CUDA8-OptiX4-OSPRay111p1.opengl.tar.gz be
# downloaded to the user's home directory

# versions to build
tag=1.9.3

# setup name variables
name="hoomd"
ver="${tag}"

# build directory
TMPROOT=$(mktemp -d /tmp/${USER}.${name}-${ver}.XXXXXX)

echo "Installing ${name}/${ver}"

# unpack binaries
cd ${TMPROOT}
tar -xvzf $HOME/vmd-${ver}.bin.LINUXAMD64-CUDA8-OptiX4-OSPRay111p1.opengl.tar.gz
cd vmd-${ver}
echo ${TMPROOT}

# set install location
root_escaped=$(sed 's/[&/\]/\\&/g' <<<"$GLOTZPORTS_ROOT")
echo "s/install_bin_dir=.*$/install_bin_dir=$root_escaped\/bin/"
sed -i "s/install_bin_dir=.*$/install_bin_dir=\"$root_escaped\/bin\";/" configure
sed -i "s/install_library_dir=.*$/install_library_dir=\"$root_escaped\/lib\/vmd\";/" configure
./configure

cd src
make install

# clean up tmp dirs
rm -rf ${TMPROOT}
