#!/bin/bash
set -e
set -u

# version to build
ver="0.5.1"

# setup name variables
name="libgetar"
repo_name="${name}"
tag="v${ver}"
git_url="ssh://git@bitbucket.org/glotzer/${repo_name}.git"

# install
pip install git+${git_url}@${tag} --no-deps --target="${GLOTZPORTS_SP}"
