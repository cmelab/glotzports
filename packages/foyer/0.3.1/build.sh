#!/bin/bash
set -e
set -u

# version to build
ver="0.3.1"

# setup name variables
name="foyer"
repo_name="${name}"
tag="v${ver}"

# install
pip install ${name}==${ver} --no-deps --target="${GLOTZPORTS_SP}"
