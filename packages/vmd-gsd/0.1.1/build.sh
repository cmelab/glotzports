#!/bin/bash
set -e
set -u

# Set the following environment vars before running build.sh
# GLOTZPORTS_ROOT - installation prefix

# versions to build
tag=v0.1.1

# setup name variables
name="gsd-vmd"
repo_name="${name}"
ver="${tag}"
git_url="https://bitbucket.org/azpanag/${repo_name}.git"
GLOTZPORTS_MAKE_JOBS="${GLOTZPORTS_MAKE_JOBS:=20}"

# build directory
TMPROOT=$(mktemp -d /tmp/${USER}.${name}-${ver}.XXXXXX)

echo "Building ${name}/${ver}"

# checkout code
git clone -n ${git_url} ${TMPROOT}/${name}
cd ${TMPROOT}/${name}
git checkout ${tag}

# configure and install hoomd
mkdir build
cd build
cmake ../ -DVMD_PLUGIN_INCLUDE_PATH=${GLOTZPORTS_ROOT}/lib/vmd/plugins/include -DVMD_PLUGIN_MOLFILE_PATH=${GLOTZPORTS_ROOT}/lib/vmd/plugins/LINUXAMD64/molfile
make install -j${GLOTZPORTS_MAKE_JOBS}

# clean up tmp dirs
rm -rf ${TMPROOT}
