#!/bin/bash
set -e
set -u

# uninstall hoomd
rm -rfv ${GLOTZPORTS_ROOT}/lib/vmd/plugins/LINUXAMD64/molfile/gsdplugin.so
