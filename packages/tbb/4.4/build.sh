#!/bin/bash
set -e
set -u

# versions to build
ver=4.4

# setup name variables
name="tbb"
fullname="tbb44_20150728oss"

# build directory
TMPROOT=$(mktemp -d /tmp/${USER}.${name}-${ver}.XXXXXX)

echo "Building ${name}/${ver} in ${TMPROOT}"

# download code
cd ${TMPROOT}
wget "https://www.threadingbuildingblocks.org/sites/default/files/software_releases/source/${fullname}_src.tgz"
tar -xzf ${fullname}_src.tgz
cd ${fullname}

# build
make arch=64 tbb_build_prefix=glotzports

# install
release_dir="build/glotzports_release"
cp "${release_dir}"/*.so* "$GLOTZPORTS_ROOT/lib"
rsync -r "include/" "${GLOTZPORTS_ROOT}/include"

# clean up tmp dirs
rm -rf ${TMPROOT}
