#!/bin/bash
set -e
set -u

# uninstall hoomd
rm -rfv ${GLOTZPORTS_ROOT}/lib/libtbb*
rm -rfv ${GLOTZPORTS_ROOT}/include/serial
rm -rfv ${GLOTZPORTS_ROOT}/include/tbb
rm -rfv ${GLOTZPORTS_ROOT}/include/index.html
