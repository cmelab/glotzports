#!/bin/bash
set -e
set -u

pip install six==1.10.0 --no-deps --prefix=${GLOTZPORTS_ROOT}
