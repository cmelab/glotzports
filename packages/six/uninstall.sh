#!/bin/bash
set -e
set -u

# Some hosts provide six in the system configuration, skip uninstallation on these systems
_provided_on="vislab"

if [[ "$_provided_on" =~ (^|[[:space:]])"${GLOTZPORTS_HOST}"($|[[:space:]]) ]]
then
    exit 0
fi

# uninstall six
pip uninstall -y six
