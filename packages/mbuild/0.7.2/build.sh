#!/bin/bash
set -e
set -u

# version to build
ver="0.7.2"

# setup name variables
name="mbuild"
repo_name="${name}"
tag="v${ver}"

# install
pip install ${name}==${ver} --no-deps --target="${GLOTZPORTS_SP}"
