#!/bin/bash
set -e
set -u

# version to build
ver="0.7.1"

# setup name variables
name="signac"
repo_name="${name}"
tag="v${ver}"
git_url="https://bitbucket.org/glotzer/${repo_name}.git"

# install
pip install git+${git_url}@${tag} --no-deps --target="${GLOTZPORTS_SP}"
