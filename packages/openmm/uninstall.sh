#!/bin/bash
set -e
set -u

# uninstall openmm
rm -rfv ${GLOTZPORTS_ROOT}/opt/openmm
rm -rfv ${GLOTZPORTS_SP}/simtk
