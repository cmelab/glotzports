#!/bin/bash
set -e
set -u

# Set the following environment vars before running build.sh
# GLOTZPORTS_SP - installation prefix python site packages dir
# GLOTZPORTS_CMAKE_FLAGS - extra configure flags to pass to cmake when configuring hoomd

# versions to build
tag=7.1.1

# setup name variables
name="openmm"
ver="${tag}"
git_url=" https://github.com/pandegroup/${name}.git"
GLOTZPORTS_MAKE_JOBS="${GLOTZPORTS_MAKE_JOBS:=20}"

# build directory
TMPROOT=$(mktemp -d /tmp/${USER}.${name}-${ver}.XXXXXX)

echo "Building ${name}/${ver}"

# checkout code
git clone -n ${git_url} ${TMPROOT}/${name}
cd ${TMPROOT}/${name}
git checkout ${tag}

# configure and install hoomd
mkdir build
cd build
eval cmake ../ -DCMAKE_INSTALL_PREFIX=${GLOTZPORTS_ROOT}/opt/openmm -DPYTHON_EXECUTABLE=${GLOTZPORTS_PYTHON} -DOPENMM_BUILD_CUDA_LIB=off -DOPENMM_BUILD_OPENCL_LIB=off -DOPENMM_BUILD_EXAMPLES=off -DOPENMM_BUILD_CUDA_COMPILER_PLUGIN=off -DBUILD_TESTING=off
make install -j${GLOTZPORTS_MAKE_JOBS}
DESTDIR=${TMPROOT}/openmm-py make PythonInstall
mv ${TMPROOT}/openmm-py/usr/lib64/python3.4/site-packages/simtk ${GLOTZPORTS_SP}

# clean up tmp dirs
rm -rf ${TMPROOT}
