#!/bin/bash
set -e
set -u

# version to build
ver="2.7.3"

# setup name variables
name="ParmEd"
repo_name="${name}"
tag="v${ver}"

# install
pip install ${name}==${ver} --no-deps --target="${GLOTZPORTS_SP}"
