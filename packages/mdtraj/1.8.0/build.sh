#!/bin/bash
set -e
set -u

# version to build
ver="1.8.0"

# setup name variables
name="mdtraj"
repo_name="${name}"
tag="v${ver}"

# install
pip install ${name}==${ver} --no-deps --target="${GLOTZPORTS_SP}"
