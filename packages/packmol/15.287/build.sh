#!/bin/bash
set -e
set -u

# Set the following environment vars before running build.sh
# GLOTZPORTS_SP - installation prefix python site packages dir
# GLOTZPORTS_CMAKE_FLAGS - extra configure flags to pass to cmake when configuring hoomd

# versions to build
tag=15.287

# setup name variables
name="packmol"
url="http://leandro.iqm.unicamp.br/packmol/versionhistory/packmol-${tag}.tar.gz"
GLOTZPORTS_MAKE_JOBS="${GLOTZPORTS_MAKE_JOBS:=20}"

# build directory
TMPROOT=$(mktemp -d /tmp/${USER}.${name}-${tag}.XXXXXX)

echo "Building ${name}/${tag}"

# checkout code
cd ${TMPROOT}
wget ${url}
tar -xvzf packmol-${tag}.tar.gz
cd packmol

# configure and install hoomd
./configure
make
cp -a packmol ${GLOTZPORTS_ROOT}/bin/

# clean up tmp dirs
rm -rf ${TMPROOT}
