# glotzports

``glotzports`` provides minimal build scripts software tools developed by the
[Glotzer Group](http://glotzerlab.engin.umich.edu/) and their dependencies, designed for use on clusters. It uses
existing admin-supported modules on the cluster when possible.

## Supported systems

* National supercomputing centers
    * Comet
    * Titan
    * Eos
    * Blue Waters

* University of Michigan
    * Glotzer group linux workstations
    * Flux

* Other systems
    * NVIDIA PSG

## Creating a glotzports environment

``glotzports`` installs all of its packages into a single environment root. You can create an empty environment:
```
./create-env /path/to/prefix ""
```

Or provide a list of packages to install when creating the environment, such as the ``all-public-packages`` list:
```
./create-env /path/to/prefix all-public-packages
```

Or specific packages:
```
./create-env /path/to/prefix "hoomd freud"
```

List the available packages with:
```
ls packages/
```

Glotzer group members can install the ``all-packages`` list which includes in-house group software. Configure
bitbucket with your ssh key to access private repositories.

## Loading the environment

To load the environment, execute:
```
source /path/to/prefix/env.sh
```
``env.sh`` will prepare your, ``PATH``, ``PYTHONPATH``, and ``LD_LIBRARY_PATH`` so that you can use the installed packages.
``env.sh`` uses ``module`` installations provided by cluster admins where possible, and assumes
that you start from a default set of loaded modules on a system.

## Installing additional software

Use the ``install`` script to install additional software not specified at environment creation time:
```
./install signac
```

You can specific specific versions of software to install:
```
./install hoomd=2.0.2
```

## Removing software

Remove software with the ``uninstall`` script:
```
./uninstall freud
```

## Updating software

``glotzports`` is a git repository. Before you update, execute:
```
git pull origin master
```
to get updated package files.

Then execute:
```
./update
```
to compile the latest versions of all packages. This may trigger a rebuild of all installed packages if the
module environment changes.

## Host specific documentation

### Comet

On Comet, you must specify either ``cpu`` or ``gpu`` when creating the environment:

```
./create-env /path/to/prefix all-public-packages cpu
```

* ``cpu``: Build all software for the CPU only for execution on CPU compute nodes.
* ``gpu``: Build CUDA-enabled software with GPU support for execution on the GPU compute nodes.
