from glotzformats.reader import GSDHOOMDFileReader
from glotzformats.writer import PosFileWriter

gsd_reader =GSDHOOMDFileReader()
with open('dump.gsd', 'rb') as gsdfile:
    traj = gsd_reader.read(gsdfile)

pos_writer = PosFileWriter()
with open('posfile.pos', 'w') as posfile:
    pos_writer.write(traj, posfile)

