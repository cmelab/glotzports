import gsd
import gsd.fl
import gsd.hoomd
import freud
import signac
import numpy

f = gsd.fl.GSDFile('dump.gsd', 'rb')
t = gsd.hoomd.HOOMDTrajectory(f)

print(numpy.mean(t[-1].particles.position))
