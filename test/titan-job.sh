#PBS -N test-glotzports
#PBS -l nodes=16,walltime=1:00:00
#PBS -A mat110
#PBS -j oe
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Change account and resource request to
# resources you have access to

# Change the glotzports root directory to
# where you installed glotzports
ROOT=/ccs/proj/mat110/software/joaander/test-env

cd $PBS_O_WORKDIR
source $ROOT/env.sh

aprun -b -N 1 -n 16 python simulate.py --mode=gpu

aprun -b -n 1 python analyze.py
aprun -b -n 1 python convert.py

echo "job done."
