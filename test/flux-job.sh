#PBS -N test-glotzports
#PBS -l nodes=4,feature=k20x,walltime=24:00:00
#PBS -l gpus=1
#PBS -A sglotzer1_fluxoe
#PBS -l qos=flux
#PBS -q fluxoe
#PBS -j oe
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Change account and resource request to 
# resources you have access to

# Change the glotzports root directory to
# where you installed glotzports
ROOT=$HOME/test-env

cd $PBS_O_WORKDIR
source $ROOT/env.sh

mpirun --bind-to none -n 4 python simulate.py --mode=gpu

python analyze.py
python convert.py

echo "job done."
