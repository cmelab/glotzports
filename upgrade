#! /bin/bash

umask 022

# ensure that glotzports is active
if [ -z ${GLOTZPORTS_ROOT+x} ]; then
    echo "Error: No active glotzports environment"
    exit 1
fi

# exit on errors
set -u
set -e

# check that the user provided arguments
if [ $# -ne 0 ]
  then
    echo "Usage: ./upgrade"
    exit 1
fi

# check if the host modules have been updated
if ! diff -q ${GLOTZPORTS_ROOT}/modules.sh system-config/`cat ${GLOTZPORTS_ROOT}/host`/modules.sh > /dev/null  2>&1; then
    echo "Updating host modules definitions and forcing rebuilds of all packages"
    echo ""
    cp system-config/`cat ${GLOTZPORTS_ROOT}/host`/modules.sh ${GLOTZPORTS_ROOT}/modules.sh
    source ${GLOTZPORTS_ROOT}/env.sh
    force_rebuild=1
else
    force_rebuild=0
fi

installed_list=$(cat ${GLOTZPORTS_ROOT}/installed-packages)

# for each package
for package in ${installed_list}
do
    # parse package=version specification
    if [[ $package =~ (.*)=(.*) ]]; then
        package_name=${BASH_REMATCH[1]}
        version=${BASH_REMATCH[2]}

        latest_version=`cat packages/$package_name/latest`

        if [ "${force_rebuild}" = "0" ] && [ "$version" = "$latest_version" ]; then
            echo "$package_name=${latest_version} is up to date"
        else
            echo "Upgrading to $package_name=${latest_version}"
            ./uninstall ${package_name}
            ./install ${package_name}=${latest_version}
            echo "Done upgrading to $package_name=${latest_version}"
            echo ""
        fi
    else
        echo "Error: syntax error in installed-packages"
        exit 1
    fi
done

if [ "${force_rebuild}" = "1" ]
then
    echo "Host module definitions were updated. Reload the glotzports environment."
fi
